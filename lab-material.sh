#!/bin/bash

rm -rf exercises
mkdir exercises
cp -r Exercises/hdfs-java-api-lab exercises
cp -r Exercises/hdfs-pseudo-distributed-lab exercises
cp -r Exercises/hive-lab exercises
cp -r Exercises/mapreduce-first-job-lab exercises
cp -r Exercises/mapreduce-running-jobs-lab exercises
cp -r Exercises/mapreduce-single-node-lab exercises
cp -r Exercises/pig-lab exercises
tar -zcvf exercises.tar.gz exercises/

rm -rf modules
mkdir modules
cp -r Modules/hdfs-java-api modules
cp -r Modules/hive modules
cp -r Modules/mapreduce-first-job modules
cp -r Modules/mapreduce-running-jobs modules
cp -r Modules/pig modules
tar -zcvf modules.tar.gz modules/

rm -rf solutions
mkdir solutions
cp -r Solutions/hdfs-java-api-lab solutions
cp -r Solutions/mapreduce-first-job-lab solutions
cp -r Solutions/mapreduce-running-jobs-lab solutions
tar -zcvf solutions.tar.gz solutions/
