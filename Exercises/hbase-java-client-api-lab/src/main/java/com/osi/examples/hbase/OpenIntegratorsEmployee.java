package com.osi.examples.hbase;

import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;

public class OpenIntegratorsEmployee implements EmployeeDAO {
    private final HTable table;
    private final static byte [] EMPLOYEE_FAMILY = toBytes("employee");
    private final static byte [] ADDRESS_FAMILY = toBytes("address");
    
    public OpenIntegratorsEmployee(Configuration conf) throws IOException {
    	table = new HTable(conf, "EmployeeAddressBook");
    }

	public void save(Employee employee) throws IOException {
		// use employee identifier as "key"
		// empoyee:first_name, employee:last_name, ....

		// TODO: Instantiate Put object with employee identifier
		
		// TODO: column key to be added only if it's not NULL. Use provided isNull()
		
		// TODO: put into table only if one of first_name, last_name or title is not NULL

		// TODO: put into table only if one of address1, address2, city, state or zip is not NULL
	}

	public Result retrieve(Employee employee) throws IOException {
		// TODO based on "key" and other column qualifiers, retrieve employee

		// TODO: column key to be added only if it's not NULL. Use provided isNull()

		return null;
	}

	public void delete(Employee employee) throws IOException {
		// TODO based on "key" delete employee
	}

	public void close() throws IOException {
		table.close();
	}

	public List<Address> getAddress(Employee employee) throws IOException {
		// we want key + first_name + last_name to locate address
		return null;
	}

	private static boolean isNull(String value) {
		if(value == null || value.equals("")) return true;
		return false;
	}
}
