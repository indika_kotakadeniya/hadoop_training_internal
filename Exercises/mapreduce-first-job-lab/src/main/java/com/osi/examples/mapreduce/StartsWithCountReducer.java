package com.osi.examples.mapreduce;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class StartsWithCountReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
    @Override
    protected void reduce(Text token, Iterable<IntWritable> counts, Context context)
    throws IOException, InterruptedException {
        int sum = 0;
        //TODO: Iterate through the "counts" Iterable object
        //TODO: Increment "sum" variable by each "count" in counts

        context.write(token, new IntWritable(sum));
    }
}
