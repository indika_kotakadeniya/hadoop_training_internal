package com.osi.examples.mapreduce;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class StartsWithCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private final static IntWritable countOne = new IntWritable(1);
    private final Text reusableText = new Text();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
    	//TODO: Create StringTokenizer using "Text" object

        //TODO: Loop through tokens in StringTokenizer
        //TODO: Insert the first character of each token into "Context" object
        //HINT: You will have to use the "reusableText" variable

    }
}
