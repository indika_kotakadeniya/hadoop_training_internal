package com.osi.examples.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class HDFSMakeDirectory {
	public static final String HDFS_FILE_PATH = "/osi_training/exercises/hdfs-java-api-lab/";
	public static final String LOCAL_FILE_PATH = "/osi_training/exercises/hdfs-java-api-lab/src/main/resources/";

	public static void main(String args[]) throws Exception {
		Configuration conf = new Configuration();
		FileSystem local = FileSystem.getLocal(conf);
		FileSystem hdfs = FileSystem.get(conf);

		System.out.println("local pwd: " + local.getHomeDirectory());
		System.out.println("hdfs pwd: " + hdfs.getHomeDirectory());
		
		String localHomeDir = local.getHomeDirectory().toString();
		String hdfsHomeDir = hdfs.getHomeDirectory().toString();

		//TODO: Create /osi_training/exercises/hdfs-java-api-lab in HDFS
		//HINT: call method off "hdfs" FileSystem Object

		//TODO: Copy html files from ~/osi_training/exercises/hdfs-java-api-lab/src/main/resources
		//to HDFS file path: /osi_training/exercises/hdfs-java-api-lab
		
		//HINT: Remember listStatus() from slide 5 in HDFS - Java API module?
		
		//HINT: Iterate through FileStatus collection
	}
}
