package com.osi.examples.hbase;

import static org.apache.hadoop.hbase.util.Bytes.toBytes;

public final class AdminAPILabConstants {
    public final static byte [] EMPLOYEE_FAMILY = toBytes("employee");
    public final static byte [] ADDRESS_FAMILY = toBytes("address");
    public final static String TABLE_NAME = "EmployeeAddressBook";
    
    private AdminAPILabConstants() {}
}
