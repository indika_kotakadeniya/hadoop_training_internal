#!/bin/bash

cd ~/.
rm -rf ~/.ssh
ssh-keygen -t dsa -P '' -f ~/.ssh/id_dsa
cat ~/.ssh/id_dsa.pub >> ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/id_dsa
chmod 600 ~/.ssh/authorized_keys

