package com.osi.examples.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class LocalMakeDirectory {
	public static final String RESOURCE_HOME = "/exercises/hdfs-java-api-lab/src/main/resources/";
	public static final String EXERCISE_HOME = "/data/hdfs-java-api-lab";

	public static void main(String args[]) throws Exception {
		Configuration conf = new Configuration();
		FileSystem local = FileSystem.getLocal(conf);
		String localHomeDir = local.getHomeDirectory().toString();

		System.out.println("pwd: " + localHomeDir);
		System.out.println("TRAINING_HOME: " + System.getenv("TRAINING_HOME"));

		//TODO: Create local sub-directory ~/osi_training/data/hdfs-java-api-lab


		//TODO: Copy files with extention "txt"
		// from: ~/osi_training/exercises/hdfs-java-api-lab/src/main/resources
		// to: ~/osi_training/data/hdfs-java-api-lab
	}
}
