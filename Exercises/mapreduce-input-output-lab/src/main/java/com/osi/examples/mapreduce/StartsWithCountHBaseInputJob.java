package com.osi.examples.mapreduce;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import static com.osi.examples.mapreduce.InputOutputModuleConstants.*;

public class StartsWithCountHBaseInputJob extends Configured implements Tool {
	//@Override
	public int run(String[] args) throws Exception {
        Job job = Job.getInstance(getConf(), "StartsWithCountHBaseInput");
        job.setJarByClass(getClass());
        // configure output and input source
        job.setInputFormatClass(TableInputFormat.class);
        
        Configuration conf = job.getConfiguration();
        HBaseConfiguration.merge(conf,
        HBaseConfiguration.create(conf));
        TableMapReduceUtil.addDependencyJars(job);

        //TODO: uncomment line below after defining constants
        //conf.set(TableInputFormat.INPUT_TABLE, TABLE_NAME);
        conf.set(TableInputFormat.SCAN_COLUMNS, "count:word");

        // configure mapper and reducer
        job.setMapperClass(StartsWithCountHBaseMapper.class);
        job.setCombinerClass(StartsWithCountHBaseReducer.class);
        job.setReducerClass(StartsWithCountHBaseReducer.class);

        // configure output
        TextOutputFormat.setOutputPath(job, new Path(args[0]));
        job.setOutputFormatClass(TextOutputFormat.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        return job.waitForCompletion(true) ? 0 : 1;
	}

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(
        new StartsWithCountHBaseInputJob(), args);
        System.exit(exitCode);
    }
}
