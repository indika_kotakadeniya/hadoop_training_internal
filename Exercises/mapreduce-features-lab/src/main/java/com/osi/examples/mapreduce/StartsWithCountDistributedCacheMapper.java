package com.osi.examples.mapreduce;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class StartsWithCountDistributedCacheMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private final static IntWritable countOne = new IntWritable(1);
    private final Text reusableText = new Text();
    private final Set<String> excludeSet = new HashSet<String>();

    public enum Tokens {
    	Total, FirstCharUpper, FirstCharLower
    }

    //TODO: exclude set should include all CAPITAL letters
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        StringTokenizer tokenizer = new StringTokenizer(value.toString());
        while (tokenizer.hasMoreTokens()) {
            String firstLetter = tokenizer.nextToken().substring(0, 1);
            if (!excludeSet.contains(firstLetter)) {
                reusableText.set(firstLetter);
                context.write(reusableText, countOne);
            }
       }
    }
}
