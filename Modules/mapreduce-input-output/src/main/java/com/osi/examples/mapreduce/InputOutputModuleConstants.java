package com.osi.examples.mapreduce;

import static org.apache.hadoop.hbase.util.Bytes.toBytes;

public class InputOutputModuleConstants {
	protected final static String TABLE_NAME = "HBaseSamples";
    protected final static byte[] FAMILY = toBytes("count");
    protected final static byte[] COLUMN = toBytes("word");
    
    private InputOutputModuleConstants() {}
}
