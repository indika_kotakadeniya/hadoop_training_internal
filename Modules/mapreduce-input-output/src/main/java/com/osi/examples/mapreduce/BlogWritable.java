package com.osi.examples.mapreduce;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

public class BlogWritable implements WritableComparable<BlogWritable>{
	private String author;
	private String content;

	public BlogWritable() {}
	
	public BlogWritable(String author, String content) {
		this.author = author;
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
    //@Override
    public void readFields(DataInput input) throws IOException {
        author = input.readUTF(); // How data is read
        content = input.readUTF();
    }

    //@Override
    public void write(DataOutput output) throws IOException {
        output.writeUTF(author); // How to write data
        output.writeUTF(content);
    }

    //@Override
    public int compareTo(BlogWritable other) {
        return author.compareTo(other.author); // How to order BlogWritables
    }
}
