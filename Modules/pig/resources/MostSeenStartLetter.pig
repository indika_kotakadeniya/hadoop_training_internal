hamletLines = LOAD '/osi_training/data/hamlet.txt' AS (line:chararray);
tokens = FOREACH hamletLines GENERATE flatten(TOKENIZE(line)) AS token:chararray;
letters = FOREACH tokens GENERATE SUBSTRING(token,0,1) AS letter:chararray;
letterGroup = GROUP letters BY letter;
countPerLetter = FOREACH letterGroup GENERATE group, COUNT(letters);
orderedCountPerLetter = ORDER countPerLetter BY $1 DESC;
result = LIMIT orderedCountPerLetter 1;
STORE result INTO '/osi_training/data/pig/output