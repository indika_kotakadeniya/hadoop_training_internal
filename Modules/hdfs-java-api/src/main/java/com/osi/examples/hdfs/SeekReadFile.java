package com.osi.examples.hdfs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

public class SeekReadFile {
    public static void main(String[] args) throws IOException {
    	FileSystem hdfs = FileSystem.get(new Configuration());
        Path fileToRead = new Path("/osi_training/exercises/hdfs-java-api/README.txt");
        FSDataInputStream input = null;
        try {
            input = hdfs.open(fileToRead);
            System.out.print("start postion = " + input.getPos() + " : ");
            IOUtils.copyBytes(input, System.out, 4096, false);

            input.seek(11);
            System.out.print("start postion = " + input.getPos() + " : ");
            IOUtils.copyBytes(input, System.out, 4096, false);

            input.seek(0);
            System.out.print("start postion = " + input.getPos() + " : ");
            IOUtils.copyBytes(input, System.out, 4096, false);
        } finally {
           IOUtils.closeStream(input);
        }
    }
}
