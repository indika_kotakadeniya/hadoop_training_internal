package com.osi.examples.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class MakeDirectory {
	public static void main(String args[]) throws Exception {
		Configuration conf = new Configuration();
		FileSystem hdfs = FileSystem.get(conf);
		Path newDirectory = new Path(hdfs.getHomeDirectory() +
			"/osi_training/exercises/HDFS-java-api-lab/sriLankanElephants");
		boolean isCreated = hdfs.mkdirs(newDirectory);
		System.out.println(newDirectory.getName() + " created: " + isCreated);
	}
}
