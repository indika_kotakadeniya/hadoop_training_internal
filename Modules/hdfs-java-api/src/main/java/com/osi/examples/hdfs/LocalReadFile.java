package com.osi.examples.hdfs;

import java.io.IOException;
import java.io.InputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

public class LocalReadFile {
    public static void main(String[] args) throws IOException {
    	FileSystem local = FileSystem.getLocal(new Configuration());
        Path fileToRead = new Path(local.getHomeDirectory() +
        	"//labs/hdfs-java-api-lab/resources/PaaKow.txt");
        InputStream input = null;
        try {
            input = local.open(fileToRead);
            IOUtils.copyBytes(input, System.out, 4096);
        } finally {
            IOUtils.closeStream(input); 
        }
    }
}
