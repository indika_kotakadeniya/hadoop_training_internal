package com.osi.examples.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class LoadConfigurations2 {
	private static final String PROP_NAME = "fs.default.name";

	public static void main(String[] args) throws Exception {
		System.out.println("user home dir: " + System.getenv("HOME"));
		Path path = new Path("/");
		if (args.length == 1){
			path = new Path(args[0]);
		}

		Configuration conf = new Configuration();
		
		System.out.println("After construction: " + conf.get(PROP_NAME));

		//conf.addResource(new Path(System.getenv("HOME") + "/conf/core-site.xml"));

		//System.out.println("After add resource: " + conf.get(PROP_NAME));

		conf.set(PROP_NAME, "hdfs://localhost:8020");
		System.out.println("After set: " + conf.get(PROP_NAME));
		
		FileSystem hdfs = FileSystem.get(conf);

		FileStatus [] files = hdfs.listStatus(path);
		for (FileStatus file : files ){
			System.out.println(file.getPath().getName());
		}
	}
}
