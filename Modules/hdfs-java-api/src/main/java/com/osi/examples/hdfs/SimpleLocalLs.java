package com.osi.examples.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class SimpleLocalLs {
	public static void main(String[] args) throws Exception{
		System.out.println("user home dir: " + System.getenv("HOME"));
		Path path = new Path(System.getenv("HOME"));
		if (args.length == 1){
			path = new Path(args[0]);
		}

		Configuration conf = new Configuration();
		FileSystem local = FileSystem.getLocal(conf);

		FileStatus [] files = local.listStatus(path);
		for (FileStatus file : files ){
			System.out.println(file.getPath().getName());
		}
	}
}
