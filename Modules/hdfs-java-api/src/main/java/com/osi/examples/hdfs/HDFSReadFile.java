package com.osi.examples.hdfs;

import java.io.IOException;
import java.io.InputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

public class HDFSReadFile {
    public static void main(String[] args) throws IOException {
    	FileSystem hdfs = FileSystem.get(new Configuration());
        Path fileToRead = new Path(hdfs.getHomeDirectory() +
        	"/osi_training/exercises/hdfs-java-api-lab/PaaKow.html");
        InputStream input = null;
        try {
            input = hdfs.open(fileToRead);
            IOUtils.copyBytes(input, System.out, 4096);
        } finally {
            IOUtils.closeStream(input); 
        }
    }
}
