package com.osi.examples.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class CopyFromLocal {
	public static final String HDFS_FILE_PATH = "/osi_training/modules/hdfs-java-api/";
	public static final String LOCAL_FILE_PATH = "/osi_training/modules/hdfs-java-api/src/main/resources/";

	public static void main(String args[]) throws Exception {
		Configuration conf = new Configuration();
		FileSystem local = FileSystem.getLocal(conf);
		FileSystem hdfs = FileSystem.get(conf);

		System.out.println("local pwd: " + local.getHomeDirectory());
		System.out.println("hdfs pwd: " + hdfs.getHomeDirectory());
		
		String localHomeDir = local.getHomeDirectory().toString();
		String hdfsHomeDir = hdfs.getHomeDirectory().toString();

		// Yes, we're going to to create /osi_training/modules/hdfs-java-api/
		if(hdfs.mkdirs(new Path(hdfsHomeDir + HDFS_FILE_PATH))) {
			System.out.println(hdfsHomeDir + HDFS_FILE_PATH + " created successfully");
		}

		//Copy hamlet.txt from ~/osi_training/modules/hdfs-java-api/src/main/resources
		//to HDFS file path: /osi_training/modules/hdfs-java-api
		hdfs.copyFromLocalFile(new Path(localHomeDir + LOCAL_FILE_PATH + "hamlet.txt"),
			new Path(hdfsHomeDir + HDFS_FILE_PATH));
	}
}
