package com.osi.examples.hdfs;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

public class WriteToFile {
    public static void main(String[] args) throws IOException {
        String message = "Sri Lankan Elephants don't have sticky poo poo!\n";
        InputStream in = new BufferedInputStream(new ByteArrayInputStream(message.getBytes()));

        Configuration conf = new Configuration();
        FileSystem hdfs = FileSystem.get(conf);
        Path toHdfs = new Path(hdfs.getHomeDirectory() +
        	"/osi_training/exercises/hdfs-java-api-lab/ElephantMissive.txt");

        FSDataOutputStream out = hdfs.create(toHdfs);

        IOUtils.copyBytes(in, out, conf);
    }
}
