package com.osi.examples.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

public class LoadConfigurations {
	private static final String PROP_NAME = "fs.default.name";

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		
		System.out.println("After construction: " + conf.get(PROP_NAME));

		conf.addResource(new Path(System.getenv("HOME") + "/conf/core-site.xml"));

		System.out.println("After add resource: " + conf.get(PROP_NAME));

		conf.set(PROP_NAME, "hdfs://localhost:8020");
		System.out.println("After set: " + conf.get(PROP_NAME));
	}
}
