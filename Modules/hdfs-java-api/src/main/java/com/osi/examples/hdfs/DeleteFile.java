package com.osi.examples.hdfs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class DeleteFile {
    public static void main(String[] args) throws IOException {
        Configuration conf = new Configuration();
        FileSystem hdfs = FileSystem.get(conf);
        Path toDelete = new Path(hdfs.getHomeDirectory() +
        	"/osi_training/exercises/HDFS-java-api-lab/ElephantMissive.txt"); // change this

        boolean isDeleted = hdfs.delete(toDelete, false);
        System.out.println(toDelete.getName() + " deleted: " + isDeleted);
    }
}
