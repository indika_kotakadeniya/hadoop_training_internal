package com.osi.examples.hdfs;

import java.util.Date;

public class Blog {
	private final String username;
	private final String blogEntry;
	private final Date created;

	public Blog(String username, String blogEntry, Date created) {
		this.username = username;
		this.blogEntry = blogEntry;
		this.created = created;
	}

	public String getUsername() {
		return username;
	}

	public String getBlogEntry() {
		return blogEntry;
	}

	public Date getCreated() {
		return created;
	}

	@Override
	public String toString() {
		return this.username + "(" + created + "): " + blogEntry ;
	}
}
