package com.osi.examples.hbase;

import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

public class JavaClientExample {
	// what do we do?
	// create addressbook table with person and address column families
	// done via shell
	// create 'EmployeeAdressBook', 'employee', 'address'
	
	// Put records
	// Get record in question
	// Modify entry
	// delete row (person)
	// drop (requires admin) -- provide method() -- move delete to admin lab
	
	public static void main(String[] args) throws IOException {
		Configuration conf = HBaseConfiguration.create();
		
		/* 
		 * Add an employee based on the following requirements:
		 * 
		 * rowKey: 01012010
		 * Insert the following to the column family: address
		 * first_name: Indika
		 * last_name: Kotakadeniya
		 * title: Professional Development Manager
		 * 
		 * Insert the following to the column family: address:
		 * address1: 345 W. Main Street
		 * address2: Suite 201
		 * city: Durham
		 * state: NC
		 * zip: 27705
		 */

		EmployeeDAO employeeDAO = new OpenIntegratorsEmployee(conf);
		Employee indika = new Employee("01012010", "Indika", "Kotakadeniya",
			"Professional Development Manager");
		Address indikaAddress = new Address("345 W. Main Street", "Suite 201", "Durham", "NC", "27705");
		System.out.println(indikaAddress);
		indika.setAddress(indikaAddress);
		employeeDAO.save(indika);
		employeeDAO.close();
		
		// Modify the zip code for employee 'Indika Kotakadeniya' to 27701
		employeeDAO = new OpenIntegratorsEmployee(conf);
		indika = new Employee();
		indika.setIdentifier("01012010");
		indikaAddress = new Address();
		indikaAddress.setZip("27701");
		indika.setAddress(indikaAddress);
		employeeDAO.save(indika);
		employeeDAO.close();

		/* 
		 * Add a second employee with the following requirements:
		 * 
		 * 	rowKey: 01012011
		 * Insert the following to the column family: address
		 * first_name: Jonathan
		 * last_name: Freeman
		 * title: Developer/Technical Evangelist
		 * Insert the following to the column family: address:
		 * address1: 119 N. Peoria Street
		 * address2: #2E
		 * city: Chicago
		 * state: IL
		 * zip: 60607
		 */

		employeeDAO = new OpenIntegratorsEmployee(conf);
		Employee jonathan = new Employee("01012011", "Jonathan", "Freeman",
			"Developer/Technical Evangelist");
		Address jonathanAddress = new Address("119 N. Peoria Street", "#2E",
			"Peoria", "IL", "60607");
		System.out.println(jonathanAddress);
		jonathan.setAddress(jonathanAddress);
		employeeDAO.save(jonathan);
		employeeDAO.close();

		// TODO: Retrieve all information for employee ‘Indika Kotakadeniya’
		employeeDAO = new OpenIntegratorsEmployee(conf);
		indika = new Employee();
		indika.setIdentifier("01012010");
		indika.setAddress(new Address());
		Result employee = employeeDAO.retrieve(indika);
		printEmployee(employee);
		employeeDAO.close();

		// TODO: Delete employees ‘Indika Kotakadeniya’ and ‘Jonathan Freeman’
		employeeDAO = new OpenIntegratorsEmployee(conf);
		indika = new Employee();
		indika.setIdentifier("01012010");
		employeeDAO.delete(indika);

		jonathan = new Employee();
		jonathan.setIdentifier("01012011");
		employeeDAO.delete(jonathan);
		employeeDAO.close();
	}
	
	private static void printEmployee(Result employee) {
	    System.out.println("--------------------------------");
	    System.out.println("RowId: " + Bytes.toString(employee.getRow()));

	    byte [] val1 = employee.getValue(toBytes("employee"), toBytes("first_name"));
	    System.out.println("employee:first_name = " + Bytes.toString(val1));

	    byte [] val2 = employee.getValue(toBytes("employee"), toBytes("last_name"));
	    System.out.println("employee:last_name = " + Bytes.toString(val2));

	    byte [] val3 = employee.getValue(toBytes("employee"), toBytes("title"));
	    System.out.println("employee:last_name = " + Bytes.toString(val3));

	    byte [] val4 = employee.getValue(toBytes("address"), toBytes("address1"));
	    System.out.println("address:address1 = " + Bytes.toString(val4));

	    byte [] val5 = employee.getValue(toBytes("address"), toBytes("address2"));
	    System.out.println("address:address2 = " + Bytes.toString(val5));

	    byte [] val6 = employee.getValue(toBytes("address"), toBytes("city"));
	    System.out.println("address:city = " + Bytes.toString(val6));

	    byte [] val7 = employee.getValue(toBytes("address"), toBytes("state"));
	    System.out.println("address:state = " + Bytes.toString(val7));

	    byte [] val8 = employee.getValue(toBytes("address"), toBytes("zip"));
	    System.out.println("address:zip = " + Bytes.toString(val8));
	}
}
