package com.osi.examples.hbase;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.hbase.client.Result;

public interface EmployeeDAO {
	public void save(Employee employee) throws IOException;
	public Result retrieve(Employee employee) throws IOException;
	public void delete(Employee employee) throws IOException;
	public void close() throws IOException;
	public List<Address> getAddress(Employee employee) throws IOException;
}
