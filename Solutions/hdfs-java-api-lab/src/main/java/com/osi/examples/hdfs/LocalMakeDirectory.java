package com.osi.examples.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class LocalMakeDirectory {
	public static final String RESOURCE_HOME = "/exercises/hdfs-java-api-lab/src/main/resources/";
	public static final String EXERCISE_HOME = "/osi_training/data/hdfs-java-api-lab";

	public static void main(String args[]) throws Exception {
		Configuration conf = new Configuration();
		FileSystem local = FileSystem.getLocal(conf);
		String localHomeDir = local.getHomeDirectory().toString();

		System.out.println("pwd: " + localHomeDir);
		System.out.println("TRAINING_HOME: " + System.getenv("TRAINING_HOME"));

		//TODO: Create local sub-directory ~/osi_training/data/hdfs-java-api-lab
		//HINT: call method off "local" FileSystem Object
		if(local.mkdirs(new Path(System.getenv("TRAINING_HOME") + EXERCISE_HOME))) {
			System.out.println(System.getenv("TRAINING_HOME") + 
				EXERCISE_HOME + " created successfully");
		}

		//TODO: Copy files with extension "txt"
		// from: ~/osi_training/exercises/hdfs-java-api-lab/src/main/resources
		// to: ~/osi_training/data/hdfs-java-api-lab
		
		//HINT: Remember listStatus() from slide 5 in HDFS - Java API module?
		FileStatus [] files = local.listStatus(new Path(System.getenv("TRAINING_HOME") + RESOURCE_HOME));
		
		//HINT: Iterate through FileStatus collection
		for (FileStatus file : files ){
			System.out.println(file.getPath().getName());
			if(file.getPath().getName().endsWith(".txt")) {
				local.copyFromLocalFile(new Path(System.getenv("TRAINING_HOME") +
					RESOURCE_HOME + file.getPath().getName()),
					new Path(System.getenv("TRAINING_HOME") + EXERCISE_HOME));
			}
		}
	}
}
