package com.osi.examples.hbase;

import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;

import static com.osi.examples.hbase.AdminAPILabConstants.*;

public class OpenIntegratorsEmployee implements EmployeeDAO {
    private final HTable table;

    public OpenIntegratorsEmployee(Configuration conf) throws IOException {
    	table = new HTable(conf, TABLE_NAME);
    }

	public void save(Employee employee) throws IOException {
		// use employee identifier as "key"
		// empoyee:first_name, employee:last_name, ....
		boolean isInsert = false;
		// TODO: Instantiate Put object with employee identifier
		Put put = new Put(toBytes(employee.getIdentifier()));
		
		// TODO: column key to be added only if it's not NULL. Use provided isNull()
		if(!isNull(employee.getFirstName()))
			put.add(EMPLOYEE_FAMILY, toBytes("first_name"), toBytes(employee.getFirstName()));
		if(!isNull(employee.getLastName()))
			put.add(EMPLOYEE_FAMILY, toBytes("last_name"), toBytes(employee.getLastName()));
		if(!isNull(employee.getTitle()))
			put.add(EMPLOYEE_FAMILY, toBytes("title"), toBytes(employee.getTitle()));
		
		isInsert = !isNull(employee.getFirstName()) || !isNull(employee.getLastName()) ||
			!isNull(employee.getTitle());
		System.out.println("isInsert: " + isInsert);
		
		// TODO: put into table only if one of first_name, last_name or title is not NULL
		if(isInsert) table.put(put);
		
		put = new Put(toBytes(employee.getIdentifier()));
		if(!isNull(employee.getAddress().getAddress1()))
			put.add(ADDRESS_FAMILY, toBytes("address1"), toBytes(employee.getAddress().getAddress1()));
		if(!isNull(employee.getAddress().getAddress2()))
			put.add(ADDRESS_FAMILY, toBytes("address2"), toBytes(employee.getAddress().getAddress2()));
		if(!isNull(employee.getAddress().getCity()))
			put.add(ADDRESS_FAMILY, toBytes("city"), toBytes(employee.getAddress().getCity()));
		if(!isNull(employee.getAddress().getState()))
			put.add(ADDRESS_FAMILY, toBytes("state"), toBytes(employee.getAddress().getState()));
		if(!isNull(employee.getAddress().getZip()))
			put.add(ADDRESS_FAMILY, toBytes("zip"), toBytes(employee.getAddress().getZip()));
		isInsert = !isNull(employee.getAddress().getAddress1()) || !isNull(employee.getAddress().getAddress2()) ||
				!isNull(employee.getAddress().getCity()) || !isNull(employee.getAddress().getState()) || !isNull(employee.getAddress().getZip());
		System.out.println("isInsert: " + isInsert);
		// TODO: put into table only if one of address1, address2, city, state or zip is not NULL
		if(isInsert) table.put(put);
	}

	public Result retrieve(Employee employee) throws IOException {
		// TODO based on "key" and other column qualifiers, retrieve employee

		Get get = new Get(toBytes(employee.getIdentifier()));

		// TODO: column key to be added only if it's not NULL. Use provided isNull()
		if(!isNull(employee.getFirstName()))
			get.addColumn(toBytes("first_name"), toBytes(employee.getFirstName()));
		if(!isNull(employee.getLastName()))
			get.addColumn(toBytes("last_name"), toBytes(employee.getLastName()));
		if(!isNull(employee.getTitle()))
			get.addColumn(toBytes("title"), toBytes(employee.getTitle()));

		if(!isNull(employee.getAddress().getAddress1()))
			get.addColumn(toBytes("address1"), toBytes(employee.getAddress().getAddress1()));
		if(!isNull(employee.getAddress().getAddress2()))
			get.addColumn(toBytes("address2"), toBytes(employee.getAddress().getAddress2()));
		if(!isNull(employee.getAddress().getCity()))
			get.addColumn(toBytes("city"), toBytes(employee.getAddress().getCity()));
		if(!isNull(employee.getAddress().getState()))
			get.addColumn(toBytes("state"), toBytes(employee.getAddress().getState()));
		if(!isNull(employee.getAddress().getZip()))
			get.addColumn(toBytes("zip"), toBytes(employee.getAddress().getZip()));

		return table.get(get);
	}

	public void delete(Employee employee) throws IOException {
		Delete delete = new Delete(toBytes(employee.getIdentifier()));
		table.delete(delete);
	}

	public void close() throws IOException {
		table.close();
	}

	public List<Address> getAddress(Employee employee) throws IOException {
		// we want key + first_name + last_name to locate address
		return null;
	}

	private static boolean isNull(String value) {
		if(value == null || value.equals("")) return true;
		return false;
	}
}
