package com.osi.examples.hbase;

import static org.apache.hadoop.hbase.util.Bytes.toBytes;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import static com.osi.examples.hbase.AdminAPILabConstants.*;

public class JavaAdminExample {
	public static void main(String[] args) throws IOException {
		Configuration conf = HBaseConfiguration.create();
		EmployeeDAO employeeDAO = null;

		createTable(conf);

		employeeDAO = new OpenIntegratorsEmployee(conf);
		Employee indika = new Employee("01012010", "Indika", "Kotakadeniya",
			"Professional Development Manager");
		Address indikaAddress = new Address("345 W. Main Street", "Suite 201", "Durham", "NC", "27705");
		System.out.println(indikaAddress);
		indika.setAddress(indikaAddress);
		employeeDAO.save(indika);
		employeeDAO.close();

		employeeDAO = new OpenIntegratorsEmployee(conf);
		indika = new Employee();
		indika.setIdentifier("01012010");
		indikaAddress = new Address();
		indikaAddress.setZip("27701");
		indika.setAddress(indikaAddress);
		employeeDAO.save(indika);
		employeeDAO.close();

		employeeDAO = new OpenIntegratorsEmployee(conf);
		Employee jonathan = new Employee("01012011", "Jonathan", "Freeman",
			"Developer/Technical Evangelist");
		Address jonathanAddress = new Address("119 N. Peoria Street", "#2E",
			"Peoria", "IL", "60607");
		System.out.println(jonathanAddress);
		jonathan.setAddress(jonathanAddress);
		employeeDAO.save(jonathan);
		employeeDAO.close();

		employeeDAO = new OpenIntegratorsEmployee(conf);
		indika = new Employee();
		indika.setIdentifier("01012010");
		indika.setAddress(new Address());
		Result employee = employeeDAO.retrieve(indika);
		printEmployee(employee);
		employeeDAO.close();

		employeeDAO = new OpenIntegratorsEmployee(conf);
		indika = new Employee();
		indika.setIdentifier("01012010");
		employeeDAO.delete(indika);

		jonathan = new Employee();
		jonathan.setIdentifier("01012011");
		employeeDAO.delete(jonathan);
		employeeDAO.close();

		deleteTable(conf);
	}
	
	private static void printEmployee(Result employee) {
	    System.out.println("--------------------------------");
	    System.out.println("RowId: " + Bytes.toString(employee.getRow()));

	    byte [] val1 = employee.getValue(toBytes("employee"), toBytes("first_name"));
	    System.out.println("employee:first_name = " + Bytes.toString(val1));

	    byte [] val2 = employee.getValue(toBytes("employee"), toBytes("last_name"));
	    System.out.println("employee:last_name = " + Bytes.toString(val2));

	    byte [] val3 = employee.getValue(toBytes("employee"), toBytes("title"));
	    System.out.println("employee:last_name = " + Bytes.toString(val3));

	    byte [] val4 = employee.getValue(toBytes("address"), toBytes("address1"));
	    System.out.println("address:address1 = " + Bytes.toString(val4));

	    byte [] val5 = employee.getValue(toBytes("address"), toBytes("address2"));
	    System.out.println("address:address2 = " + Bytes.toString(val5));

	    byte [] val6 = employee.getValue(toBytes("address"), toBytes("city"));
	    System.out.println("address:city = " + Bytes.toString(val6));

	    byte [] val7 = employee.getValue(toBytes("address"), toBytes("state"));
	    System.out.println("address:state = " + Bytes.toString(val7));

	    byte [] val8 = employee.getValue(toBytes("address"), toBytes("zip"));
	    System.out.println("address:zip = " + Bytes.toString(val8));
	}
	
	// TODO: implement this method
	public static void createTable(Configuration conf) throws IOException {
		System.out.println("calling createTable()");
		HBaseAdmin admin = new HBaseAdmin(conf);
		HTableDescriptor tableDes = new HTableDescriptor(toBytes(TABLE_NAME));
	    HColumnDescriptor employee = new HColumnDescriptor(EMPLOYEE_FAMILY);
	    HColumnDescriptor address = new HColumnDescriptor(ADDRESS_FAMILY);
	    tableDes.addFamily(employee);
	    tableDes.addFamily(address);
	    if(!admin.tableExists(toBytes(TABLE_NAME))) {
	    	System.out.println(TABLE_NAME + " doesn't exist!");
	    	admin.createTable(tableDes);
	    }
		admin.close();
	}

	// TODO: implement this method
	public static void deleteTable(Configuration conf) throws IOException {
		HBaseAdmin admin = new HBaseAdmin(conf);
		admin.disableTable(TABLE_NAME);
		admin.deleteTable(toBytes(TABLE_NAME));
		admin.close();
	}
}
